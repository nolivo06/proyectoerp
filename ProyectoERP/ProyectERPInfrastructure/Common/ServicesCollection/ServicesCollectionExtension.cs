﻿

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using ProyectoERPInfrastructure.Common.Options;
using ProyectoERPInfrastructure.Common.Options.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoERPInfrastructure.Common.ServicesCollection
{
    public static class ServicesCollectionExtension
    {
        public static void ConfigureWritable<T>(
            this IServiceCollection services,
            IConfigurationSection section,
            string file = "appsetting.json") where T : class, new()
        {
            services.Configure<T>(section);

            services.AddTransient<IWritableOptions<T>>(provider =>
            {
                var configuration = (IConfigurationRoot)provider.GetService<IConfiguration>();
                var enviroment = provider.GetService<IWebHostEnvironment>();
                var options = provider.GetService<IOptionsMonitor<T>>();
                file = enviroment.EnvironmentName == "Development" ? "appsettings.Development.json" : file;
                return new WritableOptions<T>(enviroment,options,configuration,section.Key,file);
            });

        }
        
        public static void AddCommonLayer(this IServiceCollection services)
        {

        }

    }
}
