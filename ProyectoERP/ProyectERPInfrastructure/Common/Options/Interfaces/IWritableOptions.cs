﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoERPInfrastructure.Common.Options.Interfaces
{
    public interface IWritableOptions<out T>: IOptions<T> where T : class
    {
        void Update(Action<T> applyChanges);
    }
}
