﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoERPCore.ViewModels.Configuration
{
    public class Submenu
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Route { get; set; }
        public bool Status { get; set; }
        public List<string> AccessRole { get; set; }
    }
}
