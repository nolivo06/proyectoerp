﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoERPCore.ViewModels.Configuration
{
    public class Widgets
    {
        public string Name { get; set; }
        public string Route { get; set; }
        public bool Show { get; set; }
    }
}
