﻿using ProyectoERPCore.ViewModels.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoERPCore.ViewModels.Configuration
{
    
    public class Config
    {
        private List<Menu> menus;
        private List<Widgets> widgets;
        public string Name { get; set; }
        public List<Menu> Menus { get
            {
                if(menus == null)
                {
                    return menus = new List<Menu>();
                }
                else
                {
                    return menus;
                }
            }
            set { menus = value; } }
        public Template Template { get; set; }
        public List<Widgets> Widgets { get
            {
                if (widgets == null)
                {
                    return widgets = new List<Widgets>();
                }
                else
                {
                    return widgets;
                }
            } set { widgets = value; }
        }
    }
}
