﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoERPCore.ViewModels.Configuration
{
    public class Menu
    {
        private List<Submenu> submenus;
        public string Code { get; set; }
        public string Name { get; set; }
        public string Route { get; set; }
        public string IconName { get; set; }
        public string IsMultiple { get; set; }
        public bool Status { get; set; }
        public List<string> AccessRole { get; set; }
        public List<Submenu> SubMenus { 
            get { 
            if(submenus is null)
                {
                    return submenus = new List<Submenu>();
                }
                else
                {
                    return submenus;
                }
            } 
            set {
                submenus = value;
                }
        }

    }
}
